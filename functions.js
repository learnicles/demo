function debugMsg(message) {
	document.write("Debug:"+message+newline);
}

function setPicture(myWord) {
	var pictureForWord = myWord.picture;
	//alert ("word :"+word.englishword);
	//alert ("Setting picture to : "+pictureForWord);
	
	//Exact same as line 13, just 2 separate steps
	img = document.getElementById("image");
	img.src = pictureForWord;
	
	//document.getElementById("image").src=pictureForWord;
}

/*
Describe what parameter is passed in and what the function does
*/
function setupButtons(word) {
	
	currentWordEnglish = word.englishword; // Comment this line
	buttonLetters = currentWordEnglish.split(""); // Comment this line
	/* Look up javascript array functions , describe sort, reverse 
	& tell me what other methods are available
	
	
	*/
	buttonLetters.sort(); 
	buttonLetters.reverse();
	
	var thisLetter; //Comment this line , tell me what var does here 
	var thisButton; //Comment this line
	
	for (var i=0;i<buttonLetters.length;i++)
	{ 
		thisLetter = buttonLetters[i]; // Comment this line
		/* 
		Tell me about document.getElementById() 
		*/
		thisButton = document.getElementById(i);
		/* 
		Tell me about style.visibility 
		*/
		thisButton.style.visibility="visible";
		/*
		Tell me about innerhtml
				
		*/
		thisButton.innerHTML=thisLetter;
		
	}
		
}

function addPoints() {
	hadPoints=currentUser.points;
	currentUser.points=currentUser.points+20;
	document.getElementById("points").innerHTML=currentUser.points;
}

function takeLife() {
	hadlives = currentUser.lives;
	currentUser.lives--;
	
	buttonIdtoHide="life"+hadlives;
	//alert("buttonIdtoHide :"+buttonIdtoHide);
	document.getElementById(buttonIdtoHide).style.visibility="hidden";	
		
	if (currentUser.lives == 0 ) {
		alert("Game Over");
	} 
}

function moveLetter(element) {
	container = document.getElementById("answer");
    container.appendChild(element);
}

function checkLetter(element) {

	var buttonNumber = element.id;
	var expectedLetter = currentWordEnglish[letterBeingChecked];
	var selectedLetter = buttonLetters[buttonNumber];
	
	//alert("checkLetter : "+element.id +"expecting :"+expectedLetter+" got:"+selectedLetter);
		
	if (expectedLetter == selectedLetter) {
		//alert("Correct");
		addPoints();
		moveLetter(element);
		letterBeingChecked++;
		
		if (letterBeingChecked == currentWordEnglish.length) {
			alert("Well done");
		}

	} else {
		//alert("Wrong");
		takeLife();
	}
	
}

function writeWord(word) {
	document.write("-writeWord-"+newline);
	//document.write("Wordnumber is : " +wordnumber +newline);
	document.write("Word irish is : " + word.irishword +newline);
	document.write("Word english is : " + word.englishword +newline );
	document.write("-------"+newline);
}

function displayUserInfo(user) {
 debugMsg("displayUserInfo pasnfaso"); 
 debugMsg("UserName : " +user.username);
 debugMsg("password : " +user.password);
 //debugMsg("school : " +user.school);
}

function writeAllWords() {
	debugMsg("-writeAllWords-");
	
	for (var i=0;i<myWords.length;i++)
	{ 
	document.write(myWords[i].englishword + newline);
	}
	
	document.write("-------");
}

function selectRandomNumber(maximum) {
   var wordnumber = Math.floor(Math.random()*4) ;
   return wordnumber ;
}

function selectRandomWordFromList() {
	randomNumber = selectRandomNumber(myWords.length);
	return myWords[randomNumber];
}